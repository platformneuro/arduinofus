
bool trigFUS = false;
bool trigBaphy = false;
bool firstTrig = true;
//int counter = 0;
long start;
long timer;
int outPin = 8;

void setup() {
  // setup Interrupt Service Routines
  attachInterrupt(digitalPinToInterrupt(3), getTrigBaphy, RISING);
  attachInterrupt(digitalPinToInterrupt(2), getTrigFUS, LOW);
  // output pin for Baphy
  pinMode(outPin,OUTPUT);
  digitalWrite(outPin,LOW);
  // setup serial
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
}

void loop() {
  
  if (trigFUS) {
    if (firstTrig){
      start = millis();
      firstTrig = false;
    }
    timer = millis() - start;
    Serial.print("F-");
    Serial.println(timer);
    trigFUS = !trigFUS;
    digitalWrite(outPin,HIGH);
    delay(1);
    digitalWrite(outPin,LOW);
  }
    if (trigBaphy) {
      if (firstTrig){
      start = millis();
      firstTrig = false;
    }
    timer = millis() - start;
    Serial.print("B-");
    Serial.println(timer);
    trigBaphy = !trigBaphy;
  }
}

// Interrupt functions
void getTrigBaphy()          
{                   
   trigBaphy = !trigBaphy;
}

void getTrigFUS()
{                   
   trigFUS = !trigFUS;
}
