
long start;
long timer;

int photodiodeState;
int photodiodePreviousState = 0;
const int photodiodePin = A0;

int photodiodeVal = 0;  // variable to store the photodiode voltage value.  AnalogRead returns a value [0 - 1024] scaled to 5 volts.
const int photodiodeThreshold = 500; // photodiode threshold.


void setup() {
  start = millis();
  // setup serial
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
}

void loop() {
  // Handle photo diode input
  photodiodeVal = analogRead(photodiodePin); // read the input on digital pin

  //display photodiode value to decide for threshold. Comment out afterward.
  Serial.println(photodiodeVal);
  
  if (photodiodeVal > photodiodeThreshold) {
    photodiodeState = 1;
  }
  else {
    photodiodeState = 0;
  }
  
  
  if (photodiodeState != photodiodePreviousState){
    timer = millis() - start;
    if (photodiodeState == 1){
      Serial.print("On-");
    }
    else {
      Serial.print("Off-");
    }
    Serial.println(timer);
  }
  photodiodePreviousState = photodiodeState;
}
