
long start;
long timer;

int photodiodeState;
int photodiodePreviousState = 0;
const int photodiodePin = 12;

void setup() {
  // setup serial
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
}

void loop() {

  // Handle photo diode input
  photodiodeState = digitalRead(photodiodePin); // read the input on digital pin
  if (photodiodeState != photodiodePreviousState){
    timer = millis() - start;
    if (photodiodeState == 1){
      Serial.print("On-");
    }
    else {
      Serial.print("Off-");
    }
    Serial.println(timer);
  }
  photodiodePreviousState = photodiodeState;
}
