
bool trigFUS = false;
bool trigBaphy = false;
bool firstTrig = true;
//int counter = 0;
long start;
long timer;
long timerfUS;
const int fUSToBaphyPin = 8;
const int fUSToBaslerPin = 7;

int photodiodeState;
int photodiodePreviousState = 0;
const int photodiodePin = A0;
int photodiodeVal = 0;  // variable to store the photodiode voltage value.  AnalogRead returns a value [0 - 1024] scaled to 5 volts.
const int photodiodeThreshold = 500; // photodiode threshold.

void setup() {
  // setup Interrupt Service Routines to catch triggers from fUS and Baphy
  attachInterrupt(digitalPinToInterrupt(3), getTrigBaphy, RISING);
  attachInterrupt(digitalPinToInterrupt(2), getTrigFUS, LOW);
  // output pin for Baphy
  pinMode(fUSToBaphyPin,OUTPUT);
  digitalWrite(fUSToBaphyPin,LOW);
  // Output pin for Basler camera
  pinMode(fUSToBaslerPin,OUTPUT);
  digitalWrite(fUSToBaslerPin,LOW);
  // setup serial
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
}

void loop() {

  // Handles Triggers from fUS
  if (trigFUS) {
    if (firstTrig){
      firstTrig = false;
      // start timer
      start = millis();
      // send trigger to Basler camera
      digitalWrite(fUSToBaslerPin,HIGH);

    }
    // Get current time
    timerfUS = millis() - start;
    Serial.print("F-");
    Serial.println(timerfUS);
    trigFUS = !trigFUS;
    digitalWrite(fUSToBaphyPin,HIGH);
    delay(1);
    digitalWrite(fUSToBaphyPin,LOW);
  }

  // Check if trigger from fUS are over
  if (millis() - timerfUS > 20000){
    // send trigger to Basler camera
      digitalWrite(fUSToBaslerPin,LOW);
    }

  // Handles triggers from Baphy
    if (trigBaphy) {
      if (firstTrig){
      start = millis();
      firstTrig = false;
    }
    timer = millis() - start;
    Serial.print("B-");
    Serial.println(timer);
    trigBaphy = !trigBaphy;
  }

  // Handle photo diode input
  photodiodeVal = analogRead(photodiodePin); // read the input on analog pin

  //display photodiode value to decide for threshold. Comment out afterward.
  //Serial.println(photodiodeVal);
  
  if (photodiodeVal > photodiodeThreshold) {
    photodiodeState = 1;
  }
  else {
    photodiodeState = 0;
  }
  if (photodiodeState != photodiodePreviousState){
    timer = millis() - start;
    if (photodiodeState == 1){
      Serial.print("On-");
    }
    else {
      Serial.print("Off-");
    }
    Serial.println(timer);
  }
  photodiodePreviousState = photodiodeState;
}

// Interrupt functions
void getTrigBaphy()          
{                   
   trigBaphy = !trigBaphy;
}

void getTrigFUS()
{                   
   trigFUS = !trigFUS;
}
