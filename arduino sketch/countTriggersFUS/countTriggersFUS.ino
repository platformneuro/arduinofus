
bool trigFUS = false;
bool trigBaphy = false;
bool firstTrig = true;
//int counter = 0;
long start;
long timer;

void setup() {
  // setup Interrupt Service Routines
  //attachInterrupt(digitalPinToInterrupt(3), getTrigBaphy, HIGH);
  attachInterrupt(digitalPinToInterrupt(2), getTrigFUS, LOW);
  // setup serial
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("Ready");
}

void loop() {
  //delay(5);
  //Serial.println("here");
  if (trigFUS) {
    if (firstTrig){
      start = millis();
      firstTrig = false;
    }
    timer = millis() - start;
    Serial.print("F-");
    Serial.println(timer);
    trigFUS = !trigFUS;
  }
    if (trigBaphy) {
      if (firstTrig){
      start = millis();
      firstTrig = false;
    }
    timer = millis() - start;
    Serial.print("B-");
    Serial.println(timer);
    trigBaphy = !trigBaphy;
  }
}

// Interrupt functions
//void getTrigBaphy()          
//{                   
//   trigBaphy = !trigBaphy;
//}
//
void getTrigFUS()
{                   
   trigFUS = !trigFUS;
}
