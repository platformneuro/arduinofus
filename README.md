# README - getTriggers.exe #

This code sets up and runs the arduino that register the triggers from the fUS machine and Baphy.

### Usage ###
On Baphy's computer, navigates to:
 arduinofus/python/dist/getTrigers
Run getTrigers.exe
The UI proposes the last animal used. Enter new animal name if needed. This name MUST match the name used in Baphy (case sensitive).
The program waits for any triggers from Baphy or the fUS machine, with a 120 second timeout. Once a trigger is detected, all triggers are logged in a csv file with a time stamp relative to the first trigger.
When no trigger is detected during 5 seconds (end of recording), the UI stops recording and prompt the user to start a new recording.
The window can be closed.

In this setup, the acquisition is controlled by the fUS machine. Set up Baphy and run getTriggers.exe BEFORE launching the fUS recording on the machine. Set the recording to last longer than the total amount of the stimuli presented with BAPHY.

The python code was compiled using [pyinstaller](https://pyinstaller.org/en/stable/index.html)

### Output ###
The recorded triggers are saved in the trigger folder, as "triggerFUS_AAAAMMDD_HH_MM_SS.csv".
A copy of these triggers is saved in the Baphy data folder, with the name of the last baphy data added there.

### Parameter file ###
The file parameters.txt must be created from parameters_template.txt and saved in:
arduinofus/python/dist/getTrigers

It has the following fields (interpreted as python code):

* animalName :    current animal name

* saveDir :       directory to save the triggers csv files

* saveDirBaphy :  directory where Baphy saves the data

* setTimout :     timeout before ending the trigger recordings

* comPort :       communication port used by the arduino. In case of conflicts, the arduino UI can help figure out the correct COM port.

### arduino sketches
The current sketch is countTriggersFUS2.ino. It can be loaded to the arduino using the arduino UI.
Beware that the arduino can be used by different setups in the fUS booth (audio stim with Baphy and visual stimulation for example). **Changing the sketch on the arduino can NOT be reversed, so be sure you know what you are doing.**

### arduino connections ###
* Inputs:
    - Triggers from fUS:    digital pin 2
    - Triggers from Baphy : digital pin 3
    - Triggers from Camera : digital pin 7
    - Triggers from photodiode : digital pin A0
* Outputs:
    - Triggers to Baphy :   digital pin 8

Have fun !