import serial
from serial.tools import list_ports
from time import time, localtime
from os import path, sep
import csv
from shutil import copy
from glob import glob
import traceback

def findArduinoPort():
    ports = list(list_ports.comports())
    comPort = []
    for port in ports:
        if port.description.startswith( "Arduino" ):
            # correct for the somewhat questionable design choice for the USB
            # description of the Arduino Uno
            comPort = port.device
            break
        if port.manufacturer is not None:
            if port.manufacturer.startswith( "Arduino" ) and \
                port.device.endswith( port.description ):
                comPort = port.device
                break
    return comPort

# read parameter file
paramFile =  open("parameters.txt","r")
parameters = ""
for line in paramFile:
    stripped_line = line.strip()
    exec(stripped_line)
    parameters += stripped_line + "\n"
paramFile.close()

# Check and change animal name if needed
inp = input("Current animal is " + animalName +", enter new animal name if needed: ")
if len(inp) > 0:
    saveDirBaphy = saveDirBaphy.replace(animalName,inp)
    parameters = parameters.replace(animalName,inp)

# Check COM port for arduino
portFail = False
if len(comPort) > 0:
    try:
        ser = serial.Serial(comPort,9600)
        ser.close()
    except:
        portFail = True
        # traceback.print_exc()
        print("wrong COM port. Trying automatic detection.")
        wrong_comPort = comPort
        comPort = findArduinoPort()
        try:
            ser = serial.Serial(comPort,9600)
            ser.close()
            print("detected COM port : {}".format(comPort))
            parameters = parameters.replace(wrong_comPort,comPort)
        except:
            print("No arduino detected. Use the arduino UI to figure the correct COM port, and change it in parameters.txt")
            print("You can close this window.")
            while True: pass

# edit param files
paramFile = open("parameters.txt","w+")
paramFile.write(parameters)
paramFile.close()
animalName = inp

baseName = "triggerFUS_"
keepRecording = False
try:
    while True:
        ser = serial.Serial(comPort,9600)
        ser.flushInput()
        ser.timeout = 120 # timeout to get first trigger
        firstTrig = True
        corrupted_serial_reads = 0

        print("Ready, waiting for the first trigger...")
        while True:
            try:
                ser_bytes = ser.readline()

                if firstTrig:
                    print("recording...")
                    firstTrig = False
                    ser.timeout = setTimout
                    if not keepRecording:
                        start = time()
                        startTag = localtime()
                        startTag = "%(year)04d%(month)02d%(day)02d_%(hour)02d%(min)02d%(sec)02d" %{"year": startTag[0],"month": startTag[1],"day": startTag[2],"hour": startTag[3],"min": startTag[4],"sec": startTag[5]}
                        fileName = baseName + startTag
                    
                if len(ser_bytes) == 0:
                    print("No triggers in the last " + str(setTimout) + " seconds, interrupting.")
                    break
                
                decoded_bytes = str(ser_bytes[0:len(ser_bytes)-2].decode("utf-8"))
                print(decoded_bytes)

                if len(decoded_bytes) > 0:
                    inputTime = time()
                    with open(path.join(saveDir,fileName + ".csv"),"a") as f:
                        writer = csv.writer(f,delimiter=",",lineterminator='\n')
                        writer.writerow([inputTime-start,decoded_bytes])
                else:
                    corrupted_serial_reads = corrupted_serial_reads + 1
            except KeyboardInterrupt:
                print("Manual intgerruption")
                break
            except:
                traceback.print_exc()
                break
        
        ser.close()
        print("This recording is done.")
        print("number of empty serial inputs : {}".format(corrupted_serial_reads))
        rep = input("Do you want to:\n - Start a new recording (1-default)\n - Continue this recording (2)\n - terminate (3)\n : ")

        if rep == "2":
            keepRecording = True
        elif rep == "1":
            keepRecording = False
        else:
            keepRecording = False

        # Copy triggers to baphy folder
        if not keepRecording:
            print("Triggers saved in " +  path.join(saveDir,fileName + ".csv"))
            try:
                #print(saveDirBaphy)
                fileList = glob(saveDirBaphy + sep + '*.m') # get all files in baphy directory
                baphyfileName = max(fileList, key=path.getctime)
                baphyfileName = baphyfileName.replace(".m","")
                term = "_triggerFUS.csv"
                savePath = path.join(saveDirBaphy,baphyfileName + term)
                
                # Safety check to avoid overwriting
                c = 0
                while True:
                    c += 1
                    if path.exists(savePath):
                        term = "_triggerFUS_{:03}.csv".format(c)
                        savePath = path.join(saveDirBaphy,baphyfileName + term)
                    else:
                        break
                copy(path.join(saveDir,fileName + ".csv"),savePath)
                print("Triggers saved in " + savePath)
            except:
                print("Failed to save triggers in Baphy.")
                traceback.print_exc()

        if rep == "3":
            break 

    print("All done. You can close this window")
except:
    traceback.print_exc()
    print("Unexpected error.")

while True: pass